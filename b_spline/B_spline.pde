// A INF4500 project 2 template, use it if you want
// Using float everywhere (for simplicity) since the drawing commands requires it

// Control points, vectors containing X, Y posisions
float[] B0 = new float[2];
float[] B1 = new float[2];
float[] B2 = new float[2];
float[] B3 = new float[2];
float[] B4 = new float[2];

// A 4x2 matrix you can use in the spline vector*matrix multiplication (you must fill it with the B0 ... B3 values first)
float[][] B = new float[4][2]; 

//Blending functions vector values you can fill in from the running parameter u in hte for loop
float[] U = new float[4]; 

// A single spline segment point to be plotted, defined by you as a function of the running parameter u
float[] P = new float[2]; 

// Just some global window scaling variables 
float scale = 120.0;
float yShift = 300.0;
float xShift = 100.0;

// Define colors to be used
color green = color(0, 255, 0);
color blue = color(0, 0, 255);
color white = color(255, 255, 255);

void setup() 
{
    // The plotting window
    size(500, 400);

    // Initial control point values
    B0[0] = 1.1; 
    B0[1] = 0.1;

    B1[0] = 0.3; 
    B1[1] = 0.5;

    B2[0] = 0.6; 
    B2[1] = 1.2;

    B3[0] = 1.2; 
    B3[1] = 1.3;

    B4[0] = 1.7; 
    B4[1] = 0.8;

}

void draw() 
{
    background(11);
    stroke(255, 255, 255);
    strokeWeight(3);

    // running u from 0 to 1
    for (float u = 0; u < 1; u += 0.01)
    { 

        U[0] = (float) (Math.pow(u, 3)) / 6;
        U[1] = (float) (- 3 * Math.pow(u, 3) + 3 * Math.pow(u, 2) + 3 * u + 1) / 6;
        U[2] = (float) (3 * Math.pow(u, 3) - 6 * Math.pow(u, 2) + 4) / 6;
        U[3] = (float) (- Math.pow(u, 3) + 3 * Math.pow(u, 2) - 3 * u + 1) / 6;

        // Fill B with current control points
        B[0] = B0;
        B[1] = B1;
        B[2] = B2;
        B[3] = B3;

        P = multiply(U, B);

        // Changing color
        stroke(green);

        plotPoint(P[0], P[1]);

        // Fill B with current control points
        B[0] = B1;
        B[1] = B2;
        B[2] = B3;
        B[3] = B4;

        P = multiply(U, B);

        // Changing color
        stroke(blue);

        plotPoint(P[0], P[1]);

        // Reverting color
        stroke(white);
    }

    plotControlPointsWithLinesInBetween();
    plotCoordinateSystem();
}

// A simple way to input/modify the positions of the control points
// Can be made more sophisticated, if you want
// First press a key 0-3 selecting the control point, then mouse click in the 
// plotting window where you want the point to be
// (mouseClicked, key, mouseX/Y are Processing built-in functions/variable)
void mouseClicked()
{  
    if (key == '0')
    {
        B0[0]=(mouseX- xShift)/scale;
        B0[1]=-(mouseY- yShift)/scale;
    }
    if (key == '1')
    {
        B1[0]=(mouseX- xShift)/scale;
        B1[1]=-(mouseY- yShift)/scale;
    }
    if (key == '2')
    {
        B2[0]=(mouseX- xShift)/scale;
        B2[1]=-(mouseY- yShift)/scale;
    }
    if (key == '3')
    {
        B3[0]=(mouseX- xShift)/scale;
        B3[1]=-(mouseY- yShift)/scale;
    }
    if (key == '4')
    {
        B4[0]=(mouseX- xShift)/scale;
        B4[1]=-(mouseY- yShift)/scale;
    }
    System.out.printf("B0(%f, %f), B1(%f, %f), B2(%f, %f), B3(%f, %f), B4(%f, %f)\n\n",
        B0[0], B0[1], B1[0], B1[1], B2[0], B2[1], B3[0], B3[1], B4[0], B4[1]);
}

void plotPoint(float x, float y)
{

    // Using the (globally) defined scaling factors: scale, xShift, yShift
    // "point()" is Processing bulit-in
    point((scale*x + xShift), -scale*y + yShift);
}

void plotControlPointsWithLinesInBetween()
{
    stroke(255, 255, 166);
    strokeWeight(2);
    
    // (a circle)
    ellipse(scale*B0[0] + xShift, -scale*B0[1] + yShift, 9, 9);  
    
    line(scale*B0[0] + xShift, -scale*B0[1] + yShift, scale*B1[0] + xShift, -scale*B1[1] + yShift);   
    ellipse(scale*B1[0] + xShift, -scale*B1[1] + yShift, 9, 9);
    
    line(scale*B1[0] + xShift, -scale*B1[1] + yShift, scale*B2[0] + xShift, -scale*B2[1] + yShift);
    ellipse(scale*B2[0] + xShift, -scale*B2[1] + yShift, 9, 9);
    
    line(scale*B2[0] + xShift, -scale*B2[1] + yShift, scale*B3[0] + xShift, -scale*B3[1] + yShift);
    ellipse(scale*B3[0] + xShift, -scale*B3[1] + yShift, 9, 9);  

    line(scale*B3[0] + xShift, -scale*B3[1] + yShift, scale*B4[0] + xShift, -scale*B4[1] + yShift);
    ellipse(scale*B4[0] + xShift, -scale*B4[1] + yShift, 9, 9); 
}

void plotCoordinateSystem()
{
    strokeWeight(2);
    stroke(88, 88, 88);
    
    // X axis
    line(scale*(-0.1) + xShift, -scale*(0) + yShift, scale*2.5 + xShift, -scale*(0) + yShift);
    // Y axis
    line(scale*(0) + xShift, -scale*(-0.1) + yShift, scale*0 + xShift, -scale*(1.5) + yShift);
}

// Vector * Matrix multiplication, returns C = U * B
float[] multiply(float[] U, float[][] B) 
{
    float[] C = new float[2];

    for (int j = 0; j < 2; j++)
        for (int k = 0; k < 4; k++)
            C[j] += (U[k] * B[k][j]);

    return C;
}